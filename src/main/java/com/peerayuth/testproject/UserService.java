/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.testproject;

import java.util.ArrayList;

/**
 *
 * @author Ow
 */
public class UserService {
    
    private static ArrayList<User> userList = new ArrayList<User>();
    
    static {
        userList.add(new User("admin", "password"));
    }
    
    
    public static boolean addUser(User user) {
        userList.add(user);
        return true;
    }
    
       public static boolean addUser(String userName, String password) {
        userList.add(new User(userName, password));
        return true;
    }
       
       public static boolean updateUser(int index, User user) {
           userList.set(index, user);
           return true;
       }
       
       public static User getUser(int index) {
           if (index>userList.size()-1) {
               return null;
           }
           return userList.get(index);
       }
    
       
       public static ArrayList<User> getUsers() {
           return userList;
       }
       
       public static ArrayList<User> searchUserName(String searchText) {
           ArrayList<User> list = new ArrayList<User>();
           for(User user: userList) {
               if(user.getUserName().startsWith(searchText))
                   list.add(user);
           }
           
           return list;
       }
       
       public static boolean delUser(User user) {
           userList.remove(user);
           return true;
       }
       
       public static boolean delUser(int index) {
           userList.remove(index);
           return true;
       }
       
       public static User login(String userName, String password) {
           for(User user: userList) {
               if (user.getUserName().equals(userName) && user.getPassword().equals(password)) {
                   return user;
               }
           }
           
           return null;
       }
       
       public static void save() {
                 
    }
       
       public static void load() {
           
       }
    
}
