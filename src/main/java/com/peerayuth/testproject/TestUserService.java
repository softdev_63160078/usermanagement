/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.testproject;

/**
 *
 * @author Ow
 */
public class TestUserService {
    public static void main(String[] args) {
        
        UserService.addUser("users2","pwd");
        System.out.println(UserService.getUsers());
        UserService.addUser(new User("users3","pwd"));
        System.out.println(UserService.getUsers());
        
        User user = UserService.getUser(2);
        System.out.println(user);
        
        user.setPassword("1234");
        UserService.updateUser(2, user);
        System.out.println(UserService.getUsers());
        
        UserService.delUser(user);
        System.out.println(UserService.getUsers());
        
    }
    
}
